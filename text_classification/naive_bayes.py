# -*- coding: utf-8 -*-

import numpy as np
from scipy.stats import sem
from sklearn.datasets import load_files
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.cross_validation import cross_val_score, KFold
from sklearn import metrics
from pymystem3 import Mystem
import re

def get_stop_words():
	# Список игнорируемых слов (предлоги, союзы, и т.п.)
	result = list(' ')
	for line in open('stopwords_ru.txt', 'r').readlines():
		result.append(line.strip())
	return result

def evaluate_cross_validation(clf, X, y, K):
	# Итератор для кросс-проверки
	# Создает K прогонов, в каждом из которых выделяется 1/K часть данных
	# Например если N=9, K=3:
	#   прогон 1: [0,1,2],[      3,4,5,6,7,8]
	#   прогон 2: [3,4,5],[0,1,2,      6,7,8]
	#   прогон 3: [6,7,8],[0,1,2,3,4,5      ]
	# 1. N, количество элементов во входных данных
	# 2. K, количество прогонов (folds)
	# 3. перемешивание
	# 4. начальное значение генератора случайных чисел
	cv = KFold(len(y), K, shuffle=True, random_state=0)

	# Кросс-проверка:
	# 1. estimator
	# 2. Входные данные
	# 3. Выходные данные
	# 4. Итератор для кросс-проверки
	scores = cross_val_score(clf, X, y, cv=cv)
	print(scores)
	print("Mean score: {0:.3f} (+/-{1:.3f})").format(np.mean(scores), sem(scores))

# Чтение входных данных
data = load_files('data')

mystem = Mystem()

for i in range(len(data.data)):
    line = mystem.lemmatize(data.data[i])
    ''.join(line)
    data.data[i] = ''.join(line)
    print(data.data[i])

# 75% от входных данных
SPLIT_PERC = 0.75
split_size = int(len(data.data)*SPLIT_PERC)

# Набор для обучения: 75% входных данных
X_train = data.data[:split_size]
y_train = data.target[:split_size]

# Набор для проверки: 25% входных данных
X_test = data.data[split_size:]
y_test = data.target[split_size:]

# Конвейер для обработки данных:
# 1. Векторизация
# 2. Байесовский классификатор
clf_7 = Pipeline([
	('vect', TfidfVectorizer(stop_words=get_stop_words())),
	('clf', MultinomialNB(alpha=0.01)),
	])

evaluate_cross_validation(clf_7, data.data, data.target, 5)

# Обучение и оценка результатов
def train_and_evaluate(clf, X_train, X_test, y_train, y_test):
    clf.fit(X_train, y_train)
    
    print "Accuracy on training set:"
    print clf.score(X_train, y_train)
    print "Accuracy on testing set:"
    print clf.score(X_test, y_test)
    y_pred = clf.predict(X_test)
    
    print "Classification Report:"
    print metrics.classification_report(y_test, y_pred)
    print "Confusion Matrix:"
    print metrics.confusion_matrix(y_test, y_pred)
    
train_and_evaluate(clf_7, X_train, X_test, y_train, y_test)
